# imports
import math



def run(filename):
    # We open the file car_data.txt
    car_data_file = open(filename, 'r' )

    # We read the file car_data.txt
    content = car_data_file.read()

    # We split data by car
    split_by_car = content.split(";")

    # Initialize list of cars
    list_cars = []
    list_cars_cleaned = []
    # For each cars we parse data
    for element in split_by_car:
        list_of_values = []

        # Handle case where no pipe is found
        if "|" in element:
            values_raw = element.split("|")
            # Retrieve car id
            car_id = values_raw[0]
            # Retrieve coordinates and time
            values_raw_list = values_raw[1].split("/")
            for value in values_raw_list:
                # Save values in list of list
                split_list_2 = value.split("*")
                if len(split_list_2) == 3: # If entry has more than 3 data points, we do not take it
                    list_of_values.append(split_list_2)


            # Before cleanup
            list_of_speeds = calculcate_speeds(list_of_values)
            average_speed = round(sum(list_of_speeds) / (len(list_of_values) - 1),2)
            print("Before cleanup, La voiture " + str(car_id) + " a une vitesse moyenne de " + str(average_speed))
            # CLEANUP
            i = 0
            while i < len(list_of_speeds):
                list_of_speeds = calculcate_speeds(list_of_values)
                if list_of_speeds[i] >= 10:
                    del list_of_values[i+1]
                i += 1

            cleaned_list_of_speeds = calculcate_speeds(list_of_values)
            cleaned_average_speed = round(sum(cleaned_list_of_speeds) / (len(list_of_values) - 1),2)
            print("After cleanup, La voiture " + str(car_id) + " a une vitesse moyenne de " + str(cleaned_average_speed))

            # Add car id if average speed is superior
            if average_speed >= 5:
                list_cars.append(car_id)
            if cleaned_average_speed >= 5:
                list_cars_cleaned.append(car_id)

        else:
            print("Une donnée ne peut pas être traitée:" + element)
    print("Les voitures qui ont une vitesse supérieure à 5 m/s sont avant cleanup :")
    print(list_cars)
    print("Les voitures qui ont une vitesse supérieure à 5 m/s sont après cleanup :")
    print(list_cars_cleaned)

    return list_cars


def calculcate_speeds(list_data_points):
    total_speeds = []
    # We calculate speed between two points
    for i in range(len(list_data_points) - 1):
        Xb = int(list_data_points[i+1][0])
        Xa = int(list_data_points[i][0])
        Yb = int(list_data_points[i+1][1])
        Ya = int(list_data_points[i][1])
        Tb = int(list_data_points[i+1][2])
        Ta = int(list_data_points[i][2])
        result = round((math.sqrt(((Xb-Xa)**2)+((Yb-Ya)**2)))/(Tb-Ta), 2)
        total_speeds.append(result)
    return total_speeds


if __name__ == '__main__':

    # first file
    print("Run with file car_data.txt")
    run(filename="car_data.txt")
    print("--------------------------------------")
    # second file
    print("Run with file car_data_errors.txt")
    run(filename="car_data_errors.txt")
